public class Producto {
    private String codigoProd;
    private String nombreProd;
    private Float precioCosto;
    private Float porcentajeGanacia;
    private Float ivaProd;
    private Float precioVenta;

        //setters
    public void setCodigoProd(String codigoProd){
        this.codigoProd=codigoProd;
    }
    public void setNombreProd(String nombreProd){
        this.nombreProd=nombreProd;
    }
    public void setPrecioCosto(Float precioCosto){
        this.precioCosto=precioCosto;
    }

    public void setPorcentajeGanancia(Float porcentajeGanancia){
        this.porcentajeGanacia=porcentajeGanancia;
    }

    public void setIvaProd(Float ivaProd){
        this.ivaProd=ivaProd;
    }
    public void setPrecioVenta(Float precioVenta){
        this.precioVenta=precioVenta;
    }

        //getters
    public String getCodigoProd(){
        return codigoProd;
    }
    public String getNombreProd(){
        return nombreProd;
    }
    public Float getPrecioCosto(){
        return precioCosto;
    }
    public Float getPorcentajeGanancia(){
        return porcentajeGanacia;
    }
    public Float getIvaProd(){
        return ivaProd;
    }
    public Float getPrecioVenta(){
        return precioVenta;
    }

        //método para mostar la información relativa al producto.
    public void mostrarInformacion(){
        System.out.println("Producto 1: " + nombreProd +" -Codigo de Producto: "+ codigoProd + " -Precio de costo: " + precioCosto + " -Porcentaje de ganancia: "+ porcentajeGanacia+"%"+" -Precio de Venta:" + precioVenta);
    }

        //Crear un metodo para determinar el precio de venta.

    public void determinoPrecioVenta (){
        precioVenta= (precioCosto + (precioCosto* (porcentajeGanacia/100)));
         //iva aplicado al producto
        precioVenta= precioVenta + (precioVenta*ivaProd);
    }
}

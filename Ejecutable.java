public class Ejecutable {
    public static void main (String [] args){
        Producto producto1 = new Producto();
        Producto producto2 = new Producto();

        producto1.setCodigoProd("0a70000B3134");
        producto1.setNombreProd("Pantalla LCD");
        producto1.setPrecioCosto(30000f);
        producto1.setPorcentajeGanancia(10f);
        producto1.setIvaProd(0.21f);  //el iva para todos los casos es fijo.
        producto1.determinoPrecioVenta();

        producto2.setCodigoProd("34f0000ft2114");
        producto2.setNombreProd("Reproductor");
        producto2.setPrecioCosto(25000f);
        producto2.setPorcentajeGanancia(10f);
        producto2.setIvaProd(0.21f);  //el iva para todos los casos es fijo.
        producto2.determinoPrecioVenta();


        producto1.mostrarInformacion();
        producto2.mostrarInformacion();

        //indicar cual de los dos precios de venta es el mayor.
        if (producto1.getPrecioVenta() > producto2.getPrecioVenta()){
            System.out.println("-El primer producto tiene mayor precio de venta");
        }else{
            System.out.println("-El segundo producto tiene el mayor precio de venta");
        }
        
    }
}
